<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['middleware'=> 'auth'],function(){
    Route::get('/inicio',[App\Http\Controllers\HomeController::class,'index'])->middleware('verified');

    Route::get('/mi_perfil',[App\Http\Controllers\PerfilController::class,'inicio'])->middleware('verified');

    Route::get('/editar_perfil', [App\Http\Controllers\PerfilController::class, 'editarPerfil'])->name('perfil')->middleware('verified');
    Route::post('/actualizar_perfil',[App\Http\Controllers\PerfilController::class, 'actualizacionDatos'])->name('actualizarPerfil')->middleware('verified');

    Route::resource("/entradas", App\Http\Controllers\EntradaController::class)->parameters(["niveles"=>"nivel"])->middleware('verified');
    Route::get('/delete_entrada/{id}',[App\Http\Controllers\EntradaController::class,'deleteEntrada'])->name('delete.entrada')->middleware('verified');
    Route::get('/forcedelete_entrada/{id}',[App\Http\Controllers\EntradaController::class,'forceDelete'])->name('forcedelete.entrada')->middleware('verified');
    Route::get('/papelera_entradas',[App\Http\Controllers\EntradaController::class, 'papeleraEntradas'])->name('papelera.entradas')->middleware('verified');
    Route::get('/restaurar_entrada/{id}',[App\Http\Controllers\EntradaController::class, 'restaurarEntrada'])->name('restaurar.entrada')->middleware('verified');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('verified');
