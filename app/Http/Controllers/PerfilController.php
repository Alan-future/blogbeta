<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Imagen;

class PerfilController extends Controller
{
    public function inicio(){
        $user = User::find(auth()->id());
        return view('websystem.perfil.index',compact('user'));
    }

    public function editarPerfil(){
        $user = User::find(auth()->id());
        return view('websystem.perfil.edit',compact('user'));
    }

    public function actualizacionDatos(Request $request){
        $this->validate(
            $request, 
            //validation
            ['nombre' => 'required'],
            //custom messages
            ['nombre.required' => 'El nombre es requerido, no puede ir vacio']
        );
        if($request->imagen == null){
            $user = User::find(auth()->id());
            $user->name = $request->nombre;
            $user->save();
            return redirect('/mi_perfil')->with('success', 'Perfil actualizado');
        }else{ 
            $file = $request->file('imagen');
            //obtenemos el nombre del archivo
            $nombre =  time()."_".$file->getClientOriginalName();
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put($nombre,  \File::get($file));
            
            $img_delete = Imagen::where('modelo_id','=',auth()->id())->where('modelo_type','=','App\Models\User')->get()->first();
            if($img_delete != null){
                unlink(storage_path('app/public/'.$img_delete->ruta));
                $img_delete->delete();
            }
            $user = User::find(auth()->id());
            $user->name = $request->nombre;
            $user->save();
            $imagen = new Imagen;
            $imagen->ruta = $nombre;
            $user->imagenes()->save($imagen);

            return redirect('/mi_perfil')->with('success', 'Perfil actualizado');
        }
    }
}
