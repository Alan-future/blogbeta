<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Entrada;
use App\Models\Imagen;

class EntradaController extends Controller
{
    public function index(){
        $entradas = Entrada::where('user_id','=',auth()->id())->get();
        return view('websystem.entrada.index',compact('entradas'));
    }

    public function create(){
        return view('websystem.entrada.create');
    }

    public function edit($id){
        $entrada = Entrada::where('slug','=',$id)->get()->first();
        return view('websystem.entrada.edit',compact('entrada'));
    }

    public function show($id){
        $entrada = Entrada::where('slug','=',$id)->get()->first();
        return view('websystem.entrada.show',compact('entrada'));
    }

    public function store(Request $request){
        $this->validate(
            $request, 
            //validation
            ['titulo' => 'required','cuerpo' => 'required'],
            //custom messages
            ['titulo.required' => 'El titulo es requerido', 'cuerpo.required' => 'El cuerpo o contenido es requerido']
        );
        $entrada = new Entrada;
        $entrada->titulo = $request->titulo;
        $entrada->cuerpo = $request->cuerpo;
        $entrada->slug = $request->titulo;
        $entrada->user_id = auth()->id();
        $entrada->save();

        if($request->imagen != null){
            $file = $request->file('imagen');
            //obtenemos el nombre del archivo
            $nombre =  time()."_".$file->getClientOriginalName();
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put($nombre,  \File::get($file));
    
            $imagen = new Imagen;
            $imagen->ruta = $nombre;
            $entrada->imagenes()->save($imagen);
        }

        return redirect('/entradas')->with('success', 'Entrada creada');
    }

    public function update(Request $request, $id){
        $this->validate(
            $request, 
            //validation
            ['titulo' => 'required','cuerpo' => 'required'],
            //custom messages
            ['titulo.required' => 'El titulo es requerido', 'cuerpo.required' => 'El cuerpo o contenido es requerido']
        );
        $entrada = Entrada::find($id);
        $entrada->titulo = $request->titulo;
        $entrada->cuerpo = $request->cuerpo;
        $entrada->slug = $request->titulo;
        $entrada->save();

        if($request->imagen != null){
            $file = $request->file('imagen');
            //obtenemos el nombre del archivo
            $nombre =  time()."_".$file->getClientOriginalName();
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put($nombre,  \File::get($file));
    
            $imagen = new Imagen;
            $imagen->ruta = $nombre;
            $entrada->imagenes()->save($imagen);
        }
       
        return redirect('/entradas')->with('success', 'Entrada actualizada');
    }

    public function deleteEntrada($id){
        Entrada::find($id)->delete();
        return redirect('/entradas')->with('success', 'Entrada reciclada (ve a la papelera si deseas restaurar)');
    }

    public function forceDelete($id){
       $entrada =  Entrada::onlyTrashed()->find($id);

       foreach($entrada->imagenes as $item){
        unlink(storage_path('app/public/'.$item->ruta));
       }

        $entrada->forceDelete();

        return redirect('/papelera_entradas')->with('success', 'Entrada eliminada permanentemente');
    }

    public function papeleraEntradas(){
        $entradas = Entrada::where('user_id','=',auth()->id())->onlyTrashed()->get();
        return view('websystem.entrada.delete',compact('entradas'));
    }

    public function restaurarEntrada($id){
        Entrada::onlyTrashed()->find($id)->restore();
        return redirect('/papelera_entradas')->with('success', 'Entrada restaurada');
    }
}
