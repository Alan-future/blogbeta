@extends('template.app')
@section('content')
<div>
@if(session()->get('success'))
  <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session()->get('success') }}  
  </div> 
  <br />
@endif
    <div class="card mb-3" style="display: table; margin: 0 auto;">
    <div class="row">
        <div class="col-md-4" >
        @if($user->imagenes == null)
            no ay imagen
        @else
            <img src="{{ asset('storage/'.$user->imagenes->ruta) }}" alt="" title="" width="150" height="200" />
        @endif
        </div>
        <div class="col-md-8">
        <div class="card-body">
            <h5 class="card-title">Nombre: {{auth()->user()->name}}</h5>
            <p class="card-text">Correo:{{auth()->user()->name}}.</p>
            <a href="{{route('perfil')}}"><button class="btn btn-warning">Editar información</button></a>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection