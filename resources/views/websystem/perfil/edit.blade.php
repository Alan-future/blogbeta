@extends('template.app')
@section('content')
<div>
<form method="POST" action="{{route('actualizarPerfil')}}" enctype="multipart/form-data">
    @csrf
    <div class="card mb-3" style="display: table; margin: 0 auto;">
        <div class="row no-gutters">
            <div>
                <div class="card-body">
                    <div>
                        <label>Nombre:</label>
                        <input type="text" placeholder="Ingresa tu nombre" name="nombre" class="form-control @error('nombre') is-invalid @enderror" value="{{$user->name}}">
                        @error('nombre')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="mt-3">Selecciona un archivo para tu foto de perfil:</label>
                        <input type="file" class="form-control-file" name="imagen" accept="image/*" >
                        <div class="text-center">
                            <button class="mt-3 btn btn-primary" type="submit">Actualizar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
@endsection