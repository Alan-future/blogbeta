@extends('template.app')
@section('content')
    BLOGS PUBLICOS<br><br>

    @if(count($entradas))
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Contenido</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach($entradas as $item)
        <tr>
            <th scope="row">{{$item->id}}</th>
            <td>{{$item->titulo}}</td>
            <td>{{$item->cuerpo}}</td>
            <td><a href="{{route('entradas.show',$item->slug)}}" class="btn btn-primary"><i class="far fa-eye"></i></a> 
            </td>
        </tr>
    @endforeach
  </tbody>
</table>
@else
Nadie ha publicado alguna entrada
@endif
@endsection