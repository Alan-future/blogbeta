@extends('template.app')
@section('content')
<div>
<form method="POST" action="{{route('entradas.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="card mb-3" style="display: table; margin: 0 auto;">
        <div class="row no-gutters">
            <div>
                <div class="card-body">
                    <div>
                        <label>Titulo:</label>
                        <input type="text" placeholder="Ingresa un titulo" name="titulo" class="form-control @error('titulo') is-invalid @enderror" value="">
                        @error('titulo')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <br><br>
                        <textarea class="ckeditor" name="cuerpo" rows="10" cols="80">
                        </textarea>

                        @error('cuerpo')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="mt-3">Selecciona un archivo para tu entrada:</label>
                        <input type="file" class="form-control-file" name="imagen" accept="image/*" >
                        <div class="text-center">
                            <button class="mt-3 btn btn-primary" type="submit">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
@endsection