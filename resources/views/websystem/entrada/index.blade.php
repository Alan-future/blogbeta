@extends('template.app')
@section('content')
@if(session()->get('success'))
  <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session()->get('success') }}  
  </div> 
  <br />
@endif
@if(count($entradas))
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Contenido</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach($entradas as $item)
        <tr>
            <th scope="row">{{$item->id}}</th>
            <td>{{$item->titulo}}</td>
            <td>{{$item->cuerpo}}</td>
            <td><a href="{{route('entradas.show',$item->slug)}}" class="btn btn-primary"><i class="far fa-eye"></i></a> 
            <a href="{{route('entradas.edit',$item->slug)}}" class="btn btn-warning"><i class="far fa-edit"></i></a>
            <a href="{{ route('delete.entrada',$item->id)}}" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
            </td>
        </tr>
    @endforeach
  </tbody>
</table>
@else
No tienes ninguna entrada creada
@endif

@endsection