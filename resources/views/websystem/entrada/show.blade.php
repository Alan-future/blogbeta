@extends('template.app')
@section('content')
<div class="card mb-3" style="display: table; margin: 0 auto;">
        <div class="row no-gutters">
            <div>
                <div class="card-body">
                    <div>
                        <label>Titulo:<span>{{$entrada->titulo}}</span></label>
                        <br><br>
                        <textarea rows="10" cols="80">
                        {{$entrada->cuerpo}}
                        </textarea>
                        <br><br>
                        @foreach($entrada->imagenes as $item)
                        <img src="{{ asset('storage/'.$item->ruta) }}" alt="" title="" width="150" height="200" />
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection